import asyncio
import logging
import time
import traceback
from json.decoder import JSONDecodeError
from typing import Any, Dict, List

from aiohttp import web
from aiohttp.web_request import Request
from aiohttp.web_response import Response
from environs import Env

logging.basicConfig(level=logging.INFO, format="%(asctime)s [%(levelname)s] %(message)s")
logger = logging.getLogger(__name__)
routes = web.RouteTableDef()
env = Env()

SERVER_PORT = env.int("SERVER_PORT", 8888)


class TaskProcessor:
    def __init__(self) -> None:
        self.queue: Dict[float, Dict[str, Any]] = {}
        self.num_list: List[int] = []
        self.stop_signal = False

    async def add_item(self, num: int, timeout: int) -> None:
        add_time = time.time()
        self.queue[add_time + timeout] = {"item_num": num, "add_time": add_time, "timeout": timeout}

    async def get_queue(self) -> Dict[float, Dict[str, Any]]:
        return self.queue

    async def process_item(self, item_key: float) -> None:
        queue_item = self.queue.get(item_key, None)
        if queue_item:
            self.num_list.append(queue_item["item_num"])
            self.queue.pop(item_key)

    async def get_items(self) -> List[int]:
        return self.num_list

    async def start_processing(self, app: Any) -> None:
        loop = asyncio.get_event_loop()
        loop.create_task(self.worker(app=app))

    async def worker(self, app: Any) -> None:
        while not self.stop_signal:
            await asyncio.sleep(0)
            while len(self.queue) < 1 and not self.stop_signal:
                await asyncio.sleep(0)
            if self.queue:
                item = sorted(self.queue)[0]
                if time.time() > item:
                    await self.process_item(item_key=item)

    async def stop_processing(self, app: Any) -> None:
        self.stop_signal = True


@routes.get("/get_queue")
async def get_queue(request: Request) -> Response:
    task_queue = await request.app["task_processor"].get_queue()
    queue = [
        {
            "index": index,
            "add_time": task_queue[item]["add_time"],
            "num": task_queue[item]["item_num"],
            "timeout": task_queue[item]["timeout"],
        }
        for index, item in enumerate(task_queue)
    ]

    return web.json_response({"queue": queue}, status=200)


@routes.get("/get_items")
async def get_items(request: Request) -> Response:
    items = await request.app["task_processor"].get_items()
    return web.json_response({"items": items}, status=200)


@routes.post("/add_item")
async def add_item(request: Request) -> Response:
    try:
        request_json = await request.json()
        await request.app["task_processor"].add_item(**request_json)
        return web.json_response({"ok": "ok"}, status=200)
    except JSONDecodeError:
        return web.json_response({"error": "The body of the request is not valid JSON"}, status=400)


def main() -> None:
    app = web.Application()
    app["task_processor"] = TaskProcessor()
    app.on_startup.append(app["task_processor"].start_processing)
    app.on_cleanup.append(app["task_processor"].stop_processing)
    app.add_routes(routes)
    web.run_app(app, port=SERVER_PORT)


if __name__ == "__main__":
    try:
        main()
    except Exception:
        format_exc = traceback.format_exc()
        logger.error("Server Exception", exc_info=format_exc)  # type: ignore
