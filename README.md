## START APP

```
python -m main
```


## ADD ITEM IN QUEUE

```
curl -X POST  --header "Content-Type: application/json" --data '{"num":1,"timeout":10}' 127.0.0.1:8888/add_item

```


## GET PROCESSED ITEMS

```
curl 127.0.0.1:8888/get_items
```


## GET QUEUE

```
curl 127.0.0.1:8888/get_queue
```
